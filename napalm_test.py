#!/usr/bin/env python3

# Script to test vscode remote containers
from napalm import get_network_driver
driver = get_network_driver('eos')
with driver('192.168.128.201', 'admin', 'admin') as device:
    print(device.get_facts())
